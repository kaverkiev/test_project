# Created by konstantinaverkiev at 2/15/20
Feature: Test Sign in action
  # Enter feature description here

  Background:
    Given Click on Account Tab

  Scenario: User try login with invalid email
    When Enter user@mailcom as login and 12345 as password
    Then Email error message is displayed

  Scenario: User try login with empty password
    When Enter user@mail.com as login and empty password
    And Click Sign In button
    Then Password error message is displayed

  Scenario: User try login with invalid password
    When Enter user@mail.com as login and 12345 as password
    And Click Sign In button
    Then Alert top message is displayed

  Scenario: User try to reset password. Login field save value
    When Enter user@mail.com as login and 12345 as password
    And User click forgot password button
    Then Email field contains user login user@mail.com

  Scenario: User try to reset password. Successful message
    When Enter expressvpn@intetics.com as login and 12345 as password
    And User click forgot password button
    Then User click Send Instructions button
    And User see Successful message with login expressvpn@intetics.com

