from features.application import Application
from selenium import webdriver
import os


def before_scenario(context, scenario):
    # context.driver = webdriver.Firefox()
    driver_location = "/Users/admin/_drivers/chromedriver"
    os.environ["webdriver.chrome.driver"] = driver_location
    context.driver = webdriver.Chrome(driver_location)
    context.driver.implicitly_wait(5)
    context.driver.get('https://www.expressvpn.com')
    context.app = Application(context.driver)


def after_scenario(context, scenario):
    context.driver.close()
