from behave import given, when, then


@given('Click on Account Tab')
def click_account(context):
    context.app.main_page.click_account_tab()


@when('Enter {login} as login and {password} as password')
def input_invalid_email(context, login, password):
    context.app.sign_in_page.input_credentials(login, password)


@when('Enter {login} as login and empty password')
def input_invalid_email(context, login):
    context.app.sign_in_page.input_credentials(login, '')


@when('Click Sign In button')
def click_sign_in_button(context):
    context.app.sign_in_page.click_sign_in_button()


@when('User click forgot password button')
def click_forgot_password(context):
    context.app.sign_in_page.click_forgot_password()


@when('User click forgot password button')
def click_forgot_password(context):
    context.app.signin_page.click_forgot_password()

@then('Email error message is displayed')
def check_email_error_message(context):
    context.app.sign_in_page.check_email_error()


@then('Password error message is displayed')
def check_email_error_message(context):
    context.app.sign_in_page.check_password_error()


@then('Alert top message is displayed')
def check_alert_message(context):
    context.app.sign_in_page.check_alert_message()


@then('Email field contains user login {login}')
def check_alert_message(context, login):
    context.app.sign_in_page.check_email_field_value(login)


@then('User click Send Instructions button')
def check_send_instructions(context):
    context.app.sign_in_page.click_send_instructions_button()


@then('User see Successful message with login {login}')
def check_successful_message(context, login):
    context.app.sign_in_page.check_success_message(login)
