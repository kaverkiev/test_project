import logging
import inspect


def logger(log_level):

    logger_name = inspect.stack()[1][3]
    logger_ = logging.getLogger(logger_name)

    # By default, log all messages
    logger_.setLevel(logging.DEBUG)

    file_handler = logging.FileHandler("automation.log", mode='a')
    file_handler.setLevel(log_level)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s: %(message)s',
                                  datefmt='%m-%d-%Y %H:%I%p')
    file_handler.setFormatter(formatter)
    logger_.addHandler(file_handler)

    return logger_

