import os
import psutil
import logging
from selenium import webdriver
import utilities.logger as lg


class WebDriverFactory:

    log = lg.logger(logging.DEBUG)

    dev_url = "https://www.dev.expressvpn.com"
    staging_url = "https://www.staging.expressvpn.com"
    prod_url = "https://www.expressvpn.com"

    def __init__(self, browser, base_url):
        self.browser = browser
        self.base_url = base_url

    def get_web_driver_instance(self):

        if self.base_url == "dev":
            # Dev environment
            base_url = self.dev_url
            print("Running tests on Dev environment")
            self.log.info("Running tests on Dev environment")
        elif self.base_url == "stage":
            # Stage environment
            base_url = self.staging_url
            print("Running tests on Stage environment")
            self.log.info("Running tests on Stage environment")
        elif self.base_url == "prod":
            # Prod environment
            base_url = self.prod_url
            print("Running tests on Production environment")
            self.log.info("Running tests on Production environment")
        else:
            print("Please define environment")
            self.log.info("Environment not defined")

        if self.browser == "iexplorer":
            # Set IE driver
            driver = webdriver.Ie()

        elif self.browser == "firefox":
            driver = webdriver.Firefox()
            driver.maximize_window()
            print("Running tests in Fifefox")
            self.log.info("Running tests on Firefox")

        elif self.browser == "chrome":
            if psutil.MACOS:
                # Set chrome driver
                driver_location = "/Users/averkiev/_drivers/chromedriver"
                os.environ["webdriver.chrome.driver"] = driver_location
                driver = webdriver.Chrome(driver_location)
                print("Running tests in Chrome on mac/linux node")
                self.log.info("Running tests in Chrome on mac/linux node")
            else:
                chromedriver = "C:/drivers/chromedriver.exe"
                os.environ["webdriver.chrome.driver"] = chromedriver
                driver = webdriver.Chrome(chromedriver)
                print("Running tests in Chrome on windows node")
                self.log.info("Running tests in Chrome on windows node")

        elif self.browser == "docker":
            driver = webdriver.Remote(command_executor="http://localhost:4444/wd/hub",
                                      desired_capabilities={'browserName': 'chrome'}
                                      )
            print("Running tests in Docker")
            self.log.info("Running tests in Docker")

        else:
            driver = webdriver.Firefox()
            driver.maximize_window()
            print("Running tests in Fifefox")
            self.log.info("Running tests on Firefox")

        # Setting Driver Implicit Time out for An Element
        # driver.implicitly_wait(3)

        driver.get(base_url)
        return driver
