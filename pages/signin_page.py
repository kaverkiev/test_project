from base.selenium_driver import SeleniumDriver


class SignInPage(SeleniumDriver):

    def __init__(self, driver):
        self.driver = driver

    # locators
    _login_field = "#email"
    _password_field = "#password"
    _email_error = "#email-error"
    _password_error = "#password-error"
    _sign_in_button = "//input[@name='commit']"
    _alert_message = "//div[@class='alert alert-danger flash-messages']/div"
    _success_message = "//div[@class='alert alert-success flash-messages']/div"
    _forgot_password_button = ".link .link-email"
    _send_instructions_button = "//input[@value='Send Instructions']"

    def input_credentials(self, login, password):
        self.send_keys(login, self._login_field, locator_type="css")
        self.send_keys(password, self._password_field, locator_type="css")

    def check_email_error(self):
        result = self.is_element_displayed(self._email_error, locator_type="css")
        assert result == True

    def check_password_error(self):
        result = self.is_element_displayed(self._password_error, locator_type="css")
        assert result == True

    def click_sign_in_button(self):
        self.element_click(self._sign_in_button)

    def check_alert_message(self):
        message = self.get_element(self._alert_message).text
        assert "Invalid email or password." in message, 'Expect alert Invalid email or password message'

    def click_forgot_password(self):
        self.element_click(self._forgot_password_button, locator_type="css")

    def check_email_field_value(self, login):
        field = self.get_element(self._login_field, locator_type="css")
        assert field.get_attribute('value') == login

    def click_send_instructions_button(self):
        self.element_click(self._send_instructions_button)

    def check_success_message(self, email):
        message = self.get_element(self._success_message).text
        expected_message = "Check " + str(email) + " for a password reset link or try another email."
        assert expected_message in message, 'Expect successful message with user email'