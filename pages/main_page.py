from base.selenium_driver import SeleniumDriver


class MainPage(SeleniumDriver):

    def __init__(self, driver):
        self.driver = driver

    # locators
    _account_tab = ".navbar-nav .li-myaccount .login"

    def click_account_tab(self):
        self.element_click(self._account_tab, locator_type="css")
