Designing an automation test infrastructure for ExpressVPN website. 

Goals
Demonstrate expert skills in: 
At least one UI automation framework 
At least one programming language 
Write clean and maintainable code 
Quickly and easily write useful test cases
Automation infrastructure design

Please try to spend no more than around 5 hours on this project.
Requirements: 
Create tests for https://www.expressvpn.com/sign-in ‘forget password’ and ‘sign in’ flow (no need to test through successful sign-in).
Use Python + Behave (BDD) + Selenium (our team speaks Python and Behave most of the time).
Write test cases that are enough to demonstrate your skills listed in the Goals section. 
Write a brief design doc on building a sustainable automation infrastructure that allows us to: 
Run tests on different browsers and operating systems
Run tests in parallel to reduce execution time

Deliverables
Keep all code confidential in the private GitHub repo provided
A clear readme with instructions for setting up the dev-environment and running the tests locally
A google doc for the design of automation infrastructure


________________________________________


I've added 5 scenarios. -> features\steps\signin_test.feature
I've used Page object model. Pages folder contains files with page's actions
In base folder I've add webdriverfactory file where I could define parameters for environment (dev, stage, prod) and browsers.
In utilities folder I've added docker-compose file with docker-grid. This the way to run in different browsers and os.
    
Suggestions:
- Reset password feature doesn't verify email for correctness (is it valid ?) and availability (is it exist ?)
- After 3 or 5 unsuccessful attempts of login, user login credentials should get locked for specific period e.g. 24 hours
